import React, { Component } from "react";
import './Post.css';
    class Post extends Component {
      constructor(props){
            super(props);
      }
      render() {
        const nickname = this.props.nickname;
        const avatar = this.props.avatar;
        const image = this.props.image;
        const caption = this.props.caption;

      return (
          <article ref="Post">
              <img className="Post-user-avatar img" src={avatar} alt={nickname} />
                <span>{nickname}</span>
              <img alt={caption} className="Post-img img" src={image} />
            <strong>{nickname}</strong>{caption}
          </article>
        );
      }
    }
    export default Post;